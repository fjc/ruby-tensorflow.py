require 'ruby.py'

# Top-level module to expose Python 'tensorflow' API to Ruby.
TensorFlow = RubyPy.import('tensorflow')

# Enhance the module's Ruby API.
module TensorFlow
  # Pass a tf.Session() to a block and auto-close it.
  def self.session(*args, &block)
    sess = self.Session.new(*args)
    return(sess) unless block
    begin; yield(sess); ensure; sess.close; end
  end
end
