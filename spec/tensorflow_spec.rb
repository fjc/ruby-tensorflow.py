# rubocop:disable Metrics/BlockLength
RSpec.describe TensorFlow do
  it 'has a version number' do
    expect(TensorFlow.__version__).not_to be nil
    expect(TensorFlow.__version__).to match(/^(?:\d+\.?)+$/)
  end

  # https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/1_Introduction/helloworld.py
  it 'should say hello' do
    tf = TensorFlow
    sess   = tf.Session.new
    text   = 'Hello, TensorFlow!'
    hello  = tf.constant text
    result = sess.run(hello)
    expect(result).to eq text
  end

  # https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/1_Introduction/basic_operations.py
  it 'should do basic arithmetic with constants' do
    tf = TensorFlow
    sess = tf.Session.new
    a    = tf.constant 2
    b    = tf.constant 3
    result = sess.run(a + b)
    expect(result).to eq 5
    result = sess.run(a * b)
    expect(result).to eq 6
  end

  # https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/1_Introduction/basic_operations.py
  it 'should do basic arithmetic with variables' do
    tf = TensorFlow
    sess   = tf.Session.new
    a      = tf.placeholder tf.int16
    b      = tf.placeholder tf.int16
    add    = tf.add(a, b)
    mul    = tf.multiply(a, b)
    result = sess.run(add, feed_dict: { a => 2, b => 3 })
    expect(result).to eq 5
    result = sess.run(add, feed_dict: { a => 3, b => 3 })
    expect(result).to eq 6
    result = sess.run(mul, feed_dict: { a => 4, b => 3 })
    expect(result).to eq 12
  end

  # https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/1_Introduction/basic_operations.py
  it 'should multiply matrices' do
    tf = TensorFlow
    sess    = tf.Session.new
    matrix1 = tf.constant([[3.0, 3.0]])
    matrix2 = tf.constant([[2.0], [2.0]])
    product = tf.matmul(matrix1, matrix2)
    result  = sess.run(product)
    expect(result).to eq [[12.0]]
  end

  # https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/2_BasicModels/kmeans.py
  it 'should classify digits using K-Means' do
    tf = TensorFlow
    sess = tf.Session.new

    np            = RubyPy.import 'numpy'
    factorization = RubyPy.import 'tensorflow.contrib.factorization'
    mnist         = RubyPy.import 'tensorflow.examples.tutorials.mnist'

    input_data  = mnist.input_data.read_data_sets('/tmp/data/', one_hot: true)
    full_data_x = input_data.train.images

    num_steps   = 15
    k           = 25
    num_classes = 10

    x = tf.placeholder tf.float32
    y = tf.placeholder tf.float32

    kmeans = factorization.KMeans.new(
      inputs: x, num_clusters: k, distance_metric: 'cosine',
      use_mini_batch: true
    )

    training_graph = kmeans.training_graph

    _, cluster_idx, scores, _, init_op, train_op = training_graph.to_a

    cluster_idx  = cluster_idx[0]
    avg_distance = tf.reduce_mean(scores)
    init_vars    = tf.global_variables_initializer

    sess.run(init_vars, feed_dict: { x => full_data_x })
    sess.run(init_op,   feed_dict: { x => full_data_x })

    d      = nil
    idx    = nil
    counts = np.zeros shape: PyCall::Tuple.new(k, num_classes)
    num_steps.times do
      _, d, idx = sess.run(
        [train_op, avg_distance, cluster_idx], feed_dict: { x => full_data_x }
      )
    end

    idx.size.times do |i|
      counts[idx[i]] += input_data.train.labels[i]
    end

    labels_map = counts.tolist.map { |c| np.argmax(c) }
    labels_map = tf.convert_to_tensor(labels_map)

    cluster_label = tf.nn.embedding_lookup(labels_map, cluster_idx)
    correct_pred  = tf.equal(cluster_label, tf.cast(tf.argmax(y, 1), tf.int32))
    accuracy_op   = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    test_x = input_data.test.images
    test_y = input_data.test.labels

    result = sess.run(accuracy_op, feed_dict: { x => test_x, y => test_y })
    expect((result * 100).to_i).to be >= 70
  end

  it 'should support session with block' do
    tf = TensorFlow
    result = tf.session do |sess|
      sess.run(tf.constant('42'))
    end
    expect(result).to eq '42'
  end
  it 'should support session without block' do
    tf = TensorFlow
    sess = tf.session
    result = sess.run(tf.constant('42'))
    sess.close
    expect(result).to eq '42'
  end
end
# rubocop:enable Metrics/BlockLength
